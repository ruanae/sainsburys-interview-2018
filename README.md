# Sainsbury's Server-side Coding Exercise October 2018

This project uses maven to build and test the application. 

The build should be called using the command

mvn clean package

This will build the code, run the unit tests and package the software, with dependencies in a single shaded jar file (sainsburys-interview-2018-1.0-SNAPSHOT-shaded.jar)

Once the jar file has been built it can be run from the command line

java -jar sainsburys-interview-2018-1.0-SNAPSHOT-shaded.jar

By default this will run the website scraper against the URL https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html although an alternative url can be passed to the scraper as a command line argument.
The resulting JSON will be written as a string to the command line (using standard out)


Further Development

There are a few areas which could be improved further with this scraper given time
- Test Coverage
-- there are only a few JUnit tests in this project which cover the basics and illustrate how additional tests could be written
- Logging
-- due to time constraints this project does not use a logging framework, such as Log4J, but just uses System.out and System.err to output the result of the scraper and errors. Including a logging framework would allow this to be handled better
- Configurable output location
-- CUrrently the scraper just outputs the resulting JSON object as a String to the command line. Ideally additional work could be done to output the resulting JSON to a file or other location