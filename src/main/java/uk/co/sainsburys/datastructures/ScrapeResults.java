package uk.co.sainsburys.datastructures;

import java.util.ArrayList;
import java.util.List;

/**
 * Top level POJO to hold Scrape Results
 * Will be marshaled into JSON by Jackson
 * <p>
 * Created by Andy Dickinson on 24/10/2018.
 */

public class ScrapeResults {

    private final List<ScrapeResult> results;
    private final Total total;

    public ScrapeResults(Total total) {
        this.total = total;
        results = new ArrayList<>();
    }

    public List<ScrapeResult> getResults() {
        return results;
    }


    public void addResult(ScrapeResult scrapeResult) {
        if (scrapeResult != null) {
            getResults().add(scrapeResult);
            if (scrapeResult.getUnitPrice() != null) {
                total.increaseTotal(scrapeResult.getUnitPrice());
            }
        }
    }

    public Total getTotal() {
        return total;
    }

}
