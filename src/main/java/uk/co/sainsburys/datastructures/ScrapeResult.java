package uk.co.sainsburys.datastructures;

import com.fasterxml.jackson.annotation.*;

import java.util.Map;
import java.util.TreeMap;

/**
 * POJO to hold details of a scraped item
 * <p>
 * Created by Andy Dickinson on 24/10/2018.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"title", "kcal_per_100g", "unit_price", "description"})
public class ScrapeResult {


    private Map<String, Object> propertyMap = new TreeMap<>();

    @JsonAnyGetter
    public Map<String, Object> getValue() {
        return propertyMap;
    }

    @JsonAnySetter
    public void setValue(String key, Object value) {
        propertyMap.put(key, value);
    }


    //These are convenience methods defined to allow us to set
    // properties we know we want in the code for consistency of key name
    @JsonIgnore
    public void setTitle(String title) {

        propertyMap.put("title", title);
    }

    @JsonIgnore
    public String getTitle() {
        return String.valueOf(propertyMap.get("title"));
    }

    @JsonIgnore
    public void setKcal(Integer kcal) {
        propertyMap.put("kcal_per_100g", kcal);
    }

    @JsonIgnore
    public Integer getKCal()
    {
        return (Integer)propertyMap.get("kcal_per_100g");
    }

    @JsonIgnore
    public void setDescription(String description) {
        propertyMap.put("description", description);
    }

    @JsonIgnore
    public String getDescription() {
        return String.valueOf(propertyMap.get("description"));
    }

    @JsonIgnore
    public Double getUnitPrice() {
        return (Double) propertyMap.get("unit_price");
    }

    @JsonIgnore
    public void setUnitPrice(Double unitPrice) {
        propertyMap.put("unit_price", unitPrice);
    }


}
