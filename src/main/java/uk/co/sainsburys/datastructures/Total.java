package uk.co.sainsburys.datastructures;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Preconditions;

/**
 * POJO to hold total details and calculate VAT
 * for Scrape Results
 *
 * Created by Andy Dickinson on 24/10/2018.
 */
public class Total {

    private double gross;
    @JsonIgnore
    private double vatRate;

    public Total() {
        this(20);
    }

    public Total(double vat) {
        Preconditions.checkArgument(vat >0, "Invalid VAT Value "+vat+". VAT must be greater than 0");
        this.vatRate = vat;

        this.gross = 0;
    }

    public void increaseTotal(double amount) {
        gross = gross + amount;
    }

    public double getGross() {
        return gross;
    }

    public double getVat() {

        double vat = (gross / (100 + vatRate)) * vatRate;

        //Round vat value to 2dp
        vat = Math.round(vat * 100.0) / 100.0;
        return vat;
    }
}
