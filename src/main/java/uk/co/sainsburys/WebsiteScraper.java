package uk.co.sainsburys;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import uk.co.sainsburys.datastructures.ScrapeResult;
import uk.co.sainsburys.datastructures.ScrapeResults;
import uk.co.sainsburys.datastructures.Total;

import java.io.IOException;

/**
 * Root WebScraper Class
 * <p>
 * Created by Andy Dickinson on 24/10/2018.
 */
public class WebsiteScraper {

    public ScrapeResults scrapeURL(String url) throws IOException {
        ScrapeResults scrapeResultList = new ScrapeResults(new Total(20));
        Document document = Jsoup.connect(url).get();

        Elements productNameElements = document.select("div.productNameAndPromotions");
        for (Element productNameAndPromotions : productNameElements) {
            ScrapeResult scrapeResult = processProduct(productNameAndPromotions);
            scrapeResultList.addResult(scrapeResult);
        }
        return scrapeResultList;
    }

    private ScrapeResult processProduct(Element productNameAndPromotions) throws IOException {
        Element link = productNameAndPromotions.select("a").first();

        if (link != null) {
            String productPageAbsoluteLink = link.attr("abs:href");

            Connection productConnection = Jsoup.connect(productPageAbsoluteLink);
            return scrapeProduct(link, productConnection);
        }
        return null;
    }

    private ScrapeResult scrapeProduct(Element link, Connection productConnection) throws IOException {
        if (productConnection != null) {
            Document productPage = productConnection.get();
            ScrapeResult scrapeResult = new ScrapeResult();
            scrapeResult.setTitle(link.text());
            scrapeResult.setDescription(getDescription(productPage));
            scrapeResult.setUnitPrice(getUnitPrice(productPage));
            scrapeResult.setKcal(getKCalPer100g(productPage));
            return scrapeResult;
        }
        return null;
    }

    private String getDescription(Document productPage) {
        String description = getDescriptionFromProductText(productPage);

        if (StringUtils.isBlank(description)) {
            description = getDescriptionFromItemTypeGroup(productPage);
        }

        return description;
    }

    private String getDescriptionFromProductText(Document productPage) {
        return getValueFromElement(productPage, "div.productText");
    }

    private String getValueFromElement(Document productPage, String s) {
        if (productPage != null) {
            Element rootElement = productPage.selectFirst(s);
            if (rootElement != null) {
                return getFirstNonBlankParagraphElement(rootElement);
            }
        }
        return null;
    }

    private String getDescriptionFromItemTypeGroup(Document productPage) {
        return getValueFromElement(productPage, "div.itemTypeGroup");
    }

    private String getFirstNonBlankParagraphElement(Element rootElement) {
        Elements descriptionElements = rootElement.select("p");

        String description = null;

        for (Element descriptionElement : descriptionElements) {
            if (!StringUtils.isBlank(descriptionElement.text())) {
                description = descriptionElement.text();
                break;
            }
        }
        return description;
    }

    private Double getUnitPrice(Document productPage) {
        Double unitPrice = null;
        if (productPage != null) {
            Elements pricePerUnitElements = productPage.select("p.pricePerUnit");
            if (pricePerUnitElements != null && !pricePerUnitElements.isEmpty()) {
                unitPrice = parseUnitPriceValue(unitPrice, pricePerUnitElements);

            }
        }
        return unitPrice;
    }

    private Double parseUnitPriceValue(Double unitPrice, Elements pricePerUnitElements) {
        Element unitPriceElement = pricePerUnitElements.first();

        String unitPriceString = unitPriceElement.text();

        //Strip currency symbol and unit text
        unitPriceString = unitPriceString.replace("£", "");
        unitPriceString = unitPriceString.replace("/unit", "");

        try {
            unitPrice = Double.valueOf(unitPriceString);
        } catch (NumberFormatException nfe) {
            System.out.println("Failed to parse unit price string " + unitPriceString);
        }
        return unitPrice;
    }

    private Integer getKCalPer100g(Document productPage) {
        Integer kcalPer100g = null;
        if (productPage != null) {
            kcalPer100g = scrapeKCalValue(productPage, kcalPer100g);
        }
        return kcalPer100g;
    }

    private Integer scrapeKCalValue(Document productPage, Integer kcalPer100g) {
        Element kcalElement = productPage.selectFirst("td.nutritionLevel1");
        if (kcalElement != null) {
            kcalPer100g = parseKCalValue(kcalElement);
        } else {
            kcalPer100g = processNutritionTable(productPage, kcalPer100g);

        }
        return kcalPer100g;
    }

    private Integer processNutritionTable(Document productPage, Integer kcalPer100g) {
        Element nutritionTableElement = productPage.selectFirst("table.nutritionTable");
        if (nutritionTableElement != null) {
            Element tableBody = nutritionTableElement.selectFirst("tbody");
            Elements tableRows = tableBody.select("tr");
            if (tableRows.size() > 1) {
                Element tableCell = tableRows.get(1).selectFirst("td");
                kcalPer100g = parseKCalValue(tableCell);
            }
        }
        return kcalPer100g;
    }

    private Integer parseKCalValue(Element kcalElement) {
        Integer kcalPer100g = null;
        if (kcalElement != null) {
            String kcalString = kcalElement.text();
            kcalString = kcalString.replace("kcal", "");
            try {
                kcalPer100g = Integer.valueOf(kcalString);
            } catch (NumberFormatException nfe) {
                System.out.println("Failed to parse kCal per 100g string " + kcalString);
            }
        }
        return kcalPer100g;
    }

}
