package uk.co.sainsburys;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import uk.co.sainsburys.datastructures.ScrapeResults;

import java.io.IOException;

/**
 * Main Application class to run Scraper
 * <p>
 * Created by Andy Dickinson on 24/10/2018.
 */
public class Main {

    public static void main(String[] args) throws JsonProcessingException {
        String targetURL = "https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";

        if (args.length > 0) {
            targetURL = args[0];
        }

        WebsiteScraper scraper = new WebsiteScraper();
        try {
            ScrapeResults scrapeResults = scraper.scrapeURL(targetURL);
            ObjectMapper mapper = new ObjectMapper();
            System.out.println(mapper.writeValueAsString(scrapeResults));
        } catch (IOException io) {
            System.err.println("Exception thrown during scraping: " + io.getMessage());
            io.printStackTrace();
        }
    }

}
