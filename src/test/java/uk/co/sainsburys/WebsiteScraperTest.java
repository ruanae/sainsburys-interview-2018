package uk.co.sainsburys;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import uk.co.sainsburys.datastructures.ScrapeResult;
import uk.co.sainsburys.datastructures.ScrapeResults;
import uk.co.sainsburys.datastructures.Total;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Jsoup.class)
public class WebsiteScraperTest {

    @Mock
    Connection connection;

    @Mock
    Connection productConnection;

    @Mock
    Document document;

    @Mock
    Document productPageDocument;

    @Mock
    Elements productNameElements;

    @Mock
    Elements productDescriptionElements;

    @Mock
    Elements pricePerUnitElements;


    @Mock
    Elements productLinkElements;

    @Mock
    Element productElement;

    @Mock
    Element productLink;

    @Mock
    Element productDescriptionElement;

    @Mock
    Element emptyParagraphElement;

    @Mock
    Element paragraphDescriptionElement;

    @Mock
    Element pricePerUnitElement;

    @Mock
    Element kcalElement;

    @Test
    public void testNoContent() throws IOException {

        mockStatic(Jsoup.class);
        when(Jsoup.connect(anyString())).thenReturn(connection);

        when(connection.get()).thenReturn(new Document("test"));

        WebsiteScraper scraper = new WebsiteScraper();
        ScrapeResults test = scraper.scrapeURL("test");

        assertNotNull(test);

        assertTrue(test.getResults().isEmpty());
    }


    @Test(expected = MalformedURLException.class)
    public void testMalformedURLExceptionIsThrown() throws IOException {

        mockStatic(Jsoup.class);
        when(Jsoup.connect(anyString())).thenReturn(connection);

        when(connection.get()).thenThrow(new MalformedURLException());

        WebsiteScraper scraper = new WebsiteScraper();
        scraper.scrapeURL("test");
    }


    @Test
    public void testGetSingleFullProperty() throws IOException {
        mockStatic(Jsoup.class);
        setupMockDocumentSingleItem();

        when(Jsoup.connect("test")).thenReturn(connection);

        when(connection.get()).thenReturn(document);

        WebsiteScraper scraper = new WebsiteScraper();
        ScrapeResults test = scraper.scrapeURL("test");

        assertNotNull(test);

        List<ScrapeResult> results = test.getResults();
        assertEquals(1, results.size());

        ScrapeResult result = results.get(0);

        assertEquals("SampleBerries", result.getTitle());
        assertEquals("Test Description", result.getDescription());

        assertEquals(5.0, result.getUnitPrice(), 0);
        assertEquals(Integer.valueOf(33), result.getKCal());


        Total total = test.getTotal();
        assertNotNull(total);
        assertEquals(5.0, total.getGross(), 0);
        assertEquals(0.83, total.getVat(), 0);

    }

    private void setupMockDocumentSingleItem() throws IOException {

        when(Jsoup.connect("productLink")).thenReturn(productConnection);
        when(productConnection.get()).thenReturn(productPageDocument);

        configureSingleProductPage();

        List<Element> elementList = new ArrayList<>();
        elementList.add(productElement);
        when(productNameElements.iterator()).thenReturn(elementList.iterator());
        when(document.select("div.productNameAndPromotions")).thenReturn(productNameElements);


    }

    private void configureSingleProductPage() {
        when(productElement.select("a")).thenReturn(productLinkElements);

        when(productLinkElements.first()).thenReturn(productLink);
        when(productLink.attr("abs:href")).thenReturn("productLink");
        when(productLink.text()).thenReturn("SampleBerries");


        when(productPageDocument.selectFirst("div.productText")).thenReturn(productDescriptionElement);
        when(productDescriptionElement.select("p")).thenReturn(productDescriptionElements);


        when(emptyParagraphElement.text()).thenReturn(null);
        when(paragraphDescriptionElement.text()).thenReturn("Test Description");

        List<Element> elementList = new ArrayList<>();
        elementList.add(emptyParagraphElement);
        elementList.add(paragraphDescriptionElement);

        when(productDescriptionElements.iterator()).thenReturn(elementList.iterator());


        when(productPageDocument.select("p.pricePerUnit")).thenReturn(pricePerUnitElements);

        when(pricePerUnitElements.first()).thenReturn(pricePerUnitElement);

        when(pricePerUnitElement.text()).thenReturn("£5/unit");


        when(productPageDocument.selectFirst("td.nutritionLevel1")).thenReturn(kcalElement);
        when(kcalElement.text()).thenReturn("33kcal");
    }
}