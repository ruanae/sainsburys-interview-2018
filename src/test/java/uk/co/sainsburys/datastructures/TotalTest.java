package uk.co.sainsburys.datastructures;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class TotalTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testTwentyPercentVATTotal() {
        Total total = new Total(20);
        total.increaseTotal(5);

        assertEquals(0.83,total.getVat(),0);
    }

    @Test
    public void testVATTotalNoGross() {
        Total total = new Total(20);
        total.increaseTotal(0);

        assertEquals(0,total.getVat(),0);
    }

    @Test
    public void testInvalidVAT() {

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Invalid VAT Value -20.0. VAT must be greater than 0");
        new Total(-20);

    }
}